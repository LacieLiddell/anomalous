import numpy as np
from not_used import ekg_data
import matplotlib.pyplot as plt

WINDOW_LEN = 32


def thresholding_algo(data, lag, threshold, influence):
    signals = np.zeros(len(data))
    filtered_data = np.array(data)
    avg_filter = [0] * len(data)
    standard_deviation_filter = [0] * len(data)
    avg_filter[lag - 1] = np.mean(data[0:lag])
    standard_deviation_filter[lag - 1] = np.std(data[0:lag])
    for i in range(lag, len(data) - 1):
        if abs(data[i] - avg_filter[i - 1]) > threshold * standard_deviation_filter[i - 1]:
            if data[i] > avg_filter[i - 1]:
                signals[i] = 10
            else:
                signals[i] = -10

            filtered_data[i] = influence * data[i] + (1 - influence) * filtered_data[i - 1]
            avg_filter[i] = np.mean(filtered_data[(i - lag):i])
            standard_deviation_filter[i] = np.std(filtered_data[(i - lag):i])
        else:
            signals[i] = 0
            filtered_data[i] = data[i]
            avg_filter[i] = np.mean(filtered_data[(i - lag):i])
            standard_deviation_filter[i] = np.std(filtered_data[(i - lag):i])

    return {"signals": np.asarray(signals), "avg_filter": np.asarray(avg_filter),
            "standard_deviation_filter": np.asarray(standard_deviation_filter)}
    # return dict(signals=np.asarray(signals),
    #             avgFilter=np.asarray(avg_filter),
    #             stdFilter=np.asarray(standard_deviation_filter))


def get_z_value(data):
    # Settings: lag = 30, threshold = 5, influence = 0
    lag = 10
    threshold = 10
    influence = 0

    # Run algo with settings from above
    return thresholding_algo(data, lag=lag, threshold=threshold, influence=influence)


def main():
    samples_number = 8000
    print("Reading data...")
    data = ekg_data.read_ekg_data("not_used/a02.dat")[0:samples_number]

    # window_rads = np.linspace(0, np.pi, WINDOW_LEN)
    # window = np.sin(window_rads) ** 2
    # print("Windowing data...")
    # windowed_segments = get_windowed_segments(data, window)
    result = get_z_value(data)
    plt.figure()
    n_plot_samples = 300
    plt.plot(data[0:n_plot_samples], label="Original EKG")
    plt.plot(result["signals"][0:n_plot_samples], label="Z-value, signals")
    plt.plot(result["avg_filter"][0:n_plot_samples], label="Z-value, avg_filter")
    plt.plot(result["standard_deviation_filter"][0:n_plot_samples], label="Z-value, standard_deviation_filter")
    plt.legend()
    plt.show()
