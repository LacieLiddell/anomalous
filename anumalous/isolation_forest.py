import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble import IsolationForest
import pandas as pd
import data_reader


def forest():
    n_samples = 100
    data = data_reader.make_data[0:n_samples]
    time_array = np.arange(n_samples)
    df = pd.DataFrame({"value": data, "time": time_array})
    df = pd.get_dummies(df)
    rs = np.random.RandomState(0)
    num = df.iloc[:, 1:]
    clf = IsolationForest(max_samples=100, random_state=rs, contamination=.1)
    clf.fit(df)
    if_scores = clf.decision_function(df)
    if_anomalies = clf.predict(df)
    if_anomalies = pd.Series(if_anomalies).replace([-1, 1], [1, 0])
    if_anomalies = num[if_anomalies == 1]
    # cmap = np.array(['white', 'red'])
    plt.scatter(df.iloc[:, 0], df.iloc[:, 1], c='white', s=20, edgecolor='k')
    plt.scatter(if_anomalies.iloc[:, 0], if_anomalies.iloc[:, 1], c='red')
    plt.xlabel('Income')
    plt.ylabel('Spend_Score')
    plt.title('Isolation Forests - Anomalies')
    plt.show()
    plt.figure(figsize=(12, 8))
    plt.hist(if_scores)
    plt.title("Histogram of  Avg  Anomaly  Scores: Lower = > More Anomalous")
    plt.show()

# old, but may be needed
# def isolation_forest():
#     rng = np.random.RandomState(42)
#
#     # Generate train data
#     X = 0.3 * rng.randn(100, 2)
#     X_train = np.r_[X + 2, X - 2]
#     # Generate some regular novel observations
#     X = 0.3 * rng.randn(20, 2)
#     X_test = np.r_[X + 2, X - 2]
#     # Generate some abnormal novel observations
#     X_outliers = rng.uniform(low=-4, high=4, size=(20, 2))
#
#     # fit the model
#     clf = IsolationForest(max_samples=100, random_state=rng)
#     clf.fit(X_train)
#     y_pred_train = clf.predict(X_train)
#     y_pred_test = clf.predict(X_test)
#     y_pred_outliers = clf.predict(X_outliers)
#
#     # plot the line, the samples, and the nearest vectors to the plane
#     xx, yy = np.meshgrid(np.linspace(-5, 5, 50), np.linspace(-5, 5, 50))
#     Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
#     Z = Z.reshape(xx.shape)
#
#     plt.title("IsolationForest")
#
#     b1 = plt.scatter(X_train[:, 0], X_train[:, 1], c='white',
#                      s=20, edgecolor='k')
#     b2 = plt.scatter(X_test[:, 0], X_test[:, 1], c='green',
#                      s=20, edgecolor='k')
#     c = plt.scatter(X_outliers[:, 0], X_outliers[:, 1], c='red',
#                     s=20, edgecolor='k')
#     plt.axis('tight')
#     plt.xlim((-5, 5))
#     plt.ylim((-5, 5))
#     plt.legend([b1, b2, c],
#                ["training observations",
#                 "new regular observations", "new abnormal observations"],
#                loc="upper left")
#     plt.show()