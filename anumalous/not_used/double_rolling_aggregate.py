from adtk.transformer import DoubleRollingAggregate
import pandas as pd
import matplotlib.pyplot as plt
from adtk.visualization import plot

import data_reader
from adtk.data import validate_series


def test():
    data = data_reader.make_data()
    data = validate_series(data)
    change_value(data)
    # shift_value(data)
    # mean_window(data)


def change_value(data):
    data_transformed = DoubleRollingAggregate(
        agg="quantile",
        agg_params={"q": [0.1, 0.5, 0.9]},
        window=50,
        diff="l2").transform(data).rename("Diff rolling quantiles")
    plot(pd.concat([data, data_transformed], axis=1))


def shift_value(data):
    data_transformed = DoubleRollingAggregate(
        agg="median",
        window=5,
        diff="diff").transform(data).rename("Diff rolling median")
    plot(pd.concat([data, data_transformed], axis=1));


def mean_window(data):
    data_transformed = DoubleRollingAggregate(
        agg="mean",
        window=(3, 1),  # The tuple specifies the left window to be 3, and right window to be 1
        diff="l1").transform(data).rename("Diff rolling mean with different window size")
    plot(pd.concat([data, data_transformed], axis=1), ts_linewidth=1, ts_markersize=3);